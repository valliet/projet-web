FROM docker.io/bitnami/wildfly:26 as wildfly

COPY standalone.xml /opt/bitnami/wildfly/standalone/configuration/standalone.xml
COPY mariadb-java-client-3.1.3.jar /opt/bitnami/wildfly/standalone/deployments/mariadb-java-client-3.1.3.jar
COPY backend/build/libs/kahoot.war /opt/bitnami/wildfly/standalone/deployments/kahoot.war

FROM node:18 as frontend

WORKDIR /app
COPY frontend/package.json /app/package.json
COPY frontend/package-lock.json /app/package-lock.json
RUN npm install

COPY frontend /app
RUN npm run build

CMD ["node", "build/index.js"]