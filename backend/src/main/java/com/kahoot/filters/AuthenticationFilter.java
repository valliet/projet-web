package com.kahoot.filters;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.json.*;

import com.kahoot.beans.Session;
import com.kahoot.beans.User;

import java.io.IOException;

@WebFilter("/rest/game/*")
public class AuthenticationFilter implements Filter {
    @PersistenceContext
    EntityManager em;

    public void init(FilterConfig config) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("Authentication filter");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        JsonObjectBuilder json = Json.createObjectBuilder();
        try {
            System.out.println("Request received");
            // Retrieve cookies from the request
            String token = req.getHeader("Authorization");
            System.out.println("Token: " + token);
            if (token != null) {
                // Check if the token is in the database and return the rights
                Session s = Session.findByToken(token, em);
                if (s == null) {
                    System.out.println("Session not found");
                    resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    resp.setContentType("application/json");
                    json.add("error", "Token not found");
                    resp.getWriter().write(json.build().toString());
                    return;
                }
                User user = s.getOwner();
                if (user != null) {
                    System.out.println("User found");
                    request.setAttribute("user", user);
                    System.out.println("URI: " + req.getRequestURI());
                    chain.doFilter(request, response);
                    return;
                }
            }
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.setContentType("application/json");
            json.add("error", "User not found");
            resp.getWriter().write(json.build().toString());
            return;
        } catch (Exception e) {
            throw new ServletException("Error processing request", e);
        } finally {
        }
    }

    public void destroy() {
    }
}