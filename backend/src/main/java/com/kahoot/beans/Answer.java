package com.kahoot.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.json.*;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Answer {

    // id
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // name
    @Column(name = "name")
    @NotNull
    private String name;

    // is_correct
    @Column(name = "is_correct")
    @NotNull
    private boolean is_correct;

    // question
    @ManyToOne
    @NotNull
    private Question question;

    // userAnswers
    @OneToMany(mappedBy = "answer")
    private List<UserAnswer> userAnswers;

    // Constructor
    public Answer(String name, boolean is_correct, Question question) {
        this.name = name;
        this.is_correct = is_correct;
        this.question = question;
    }

    // ToJson
    public JsonObject toJson() {
        return Json.createObjectBuilder()
            .add("id", this.id)
            .add("answer", this.name)
            .add("is_correct", this.is_correct)
            .build();
    }
}
