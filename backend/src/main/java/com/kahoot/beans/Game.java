package com.kahoot.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import java.time.OffsetDateTime;

import java.util.List;
import javax.json.*;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import com.fasterxml.uuid.Generators;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Game {

    public enum Status {
        CREATED, STARTED, PLAYING, FINISHED
    }

    // id
    @Id
    @Column(name = "uuid")
    @NotNull
    private String id;

    // name
    @Column(name = "name")
    @NotNull
    private String name;

    // owner
    @ManyToOne
    @NotNull
    private User owner;

    // current question
    @Column(name = "current_question")
    private int current_question = 0;

    // Link to questions
    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Question> questions;

    // status
    @Column(name = "status")
    @NotNull
    private Status status = Status.CREATED;

    // date
    @Column(name = "date")
    private OffsetDateTime date;

    // players
    @ManyToMany()
    private List<User> players;

    // Constructor

    public Game(String name, User owner) {
        this.id = Generators.timeBasedGenerator().generate().toString();
        this.name = name;
        this.owner = owner;
        this.status = Status.CREATED;
    }

    // To JSON
    public JsonObject toJson() {
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("id", id);
        json.add("name", name);
        json.add("owner", owner.toJson());
        json.add("status", status.toString());
        json.add("current_question", current_question);
        if (date != null) {
            json.add("date", date.toString());
        }
        // Add all questions
        JsonArrayBuilder questionsJson = Json.createArrayBuilder();
        for (Question question : questions) {
            questionsJson.add(question.toJson());
        }
        json.add("questions", questionsJson.build());

        return json.build();
    }
}
