package com.kahoot.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import java.time.OffsetDateTime ;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.json.*;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Question {

    // id
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // question
    @Column(name = "question")
    @NotNull
    private String question;

    // game
    @ManyToOne
    @NotNull
    private Game game;

    // Link to answers
    @OneToMany(mappedBy = "question", cascade = CascadeType.REMOVE)
    private List<Answer> answers;

    // time_limit
    @Column(name = "time_limit")
    private int time_limit = 30;

    @Column(name = "date")
    private OffsetDateTime date;

    // order
    @Column(name = "number")
    @NotNull
    private int order;

    // Constructor
    public Question(String question, int order, Game game) {
        this.question = question;
        this.game = game;
        this.order = order;
    }

    public Question(String question, int order, Game game, int time_limit) {
        this.question = question;
        this.game = game;
        this.time_limit = time_limit;
        this.order = order;
    }

    // findByOrder
    public static Question findByOrder(int order, Game Game, EntityManager em) {
        TypedQuery<Question> query = em.createQuery("SELECT q FROM Question q WHERE q.order = :order AND q.game = :game", Question.class);
        query.setParameter("order", order);
        query.setParameter("game", Game);
        List<Question> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    // To JSON
    public JsonObject toJson() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("id", this.id);
        builder.add("question", this.question);
        builder.add("time_limit", this.time_limit);
        builder.add("order", this.order);

        // Add answers
        JsonArrayBuilder answersJson = Json.createArrayBuilder();
        for (Answer answer : answers) {
            answersJson.add(answer.toJson());
        }
        builder.add("answers", answersJson.build());
        return builder.build();
    }
}
