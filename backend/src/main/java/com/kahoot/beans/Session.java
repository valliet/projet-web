package com.kahoot.beans;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.security.SecureRandom;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Session {

    // id
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // owner
    @ManyToOne
    @NotNull
    private User owner;

    // token
    @Column(name = "token")
    @NotNull
    private String token;

    // expires
    @Column(name = "expires")
    private String expires;

    // Generate a random token
    private String generateToken() {
        byte[] bytes = new byte[128];
        new SecureRandom().nextBytes(bytes);
        return bytes.toString();
    }

    // Constructor
    public Session(User owner) {
        this.owner = owner;
        this.token = generateToken();
    }

    // Find a session by token
    public static Session findByToken(String token, EntityManager em) {
        TypedQuery<Session> query = em.createQuery("SELECT s FROM Session s WHERE s.token = :token", Session.class);
        query.setParameter("token", token);
        List<Session> results = query.getResultList();
        if (results.size() == 0) {
            return null;
        }
        return results.get(0);
    }
}
