package com.kahoot.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.time.OffsetDateTime ;

import javax.validation.constraints.NotNull;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class UserAnswer {

    // id
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // user
    @ManyToOne
    @NotNull
    private User user;

    // answer
    @ManyToOne
    @NotNull
    private Answer answer;

    // date
    @Column(name = "date")
    private OffsetDateTime date = OffsetDateTime.now();

    // Contructor
    public UserAnswer(User user, Answer answer) {
        this.user = user;
        this.answer = answer;
    }
}
