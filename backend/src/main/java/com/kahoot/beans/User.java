package com.kahoot.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import java.util.List;

import javax.json.*;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {

    // uid
    @Id
    @Column(name = "uid")
    @NotNull
    private String uid;

    // display_name
    @Column(name = "display_name")
    @NotNull
    private String display_name;

    // is_admin
    @Column(name = "is_admin")
    private boolean is_admin = false;

    // Link to games
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private List<Game> games;

    // Link to all played games
    @ManyToMany(mappedBy = "players", cascade = CascadeType.ALL)
    private List<Game> played_games;

    // Link to sessions
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private List<Session> sessions;

    // Link to userAnswers
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<UserAnswer> userAnswers;

    // Constructor
    
    public User(String uid, String display_name) {
        this.uid = uid;
        this.display_name = display_name;
    }

    // To JSON
    public JsonObject toJson() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("uid", this.uid);
        builder.add("display_name", this.display_name);
        builder.add("is_admin", this.is_admin);
        return builder.build();
    }
}
