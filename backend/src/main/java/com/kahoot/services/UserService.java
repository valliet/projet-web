package com.kahoot.services;

import javax.annotation.processing.Generated;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.json.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.MalformedURLException;
import java.io.IOException;
import javax.ws.rs.core.MediaType;
import java.io.StringReader;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.NewCookie;

// import User Bean and Session Bean
import com.kahoot.beans.User;
import com.kahoot.beans.Session;

@Singleton
@Path("/user")
public class UserService {

    String CAS_URL = System.getenv("CAS_URL");
    String CAS_SERVICE = System.getenv("CAS_SERVICE");

    @PersistenceContext
    private EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest req) {
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Get the authentication bearer token from the header
        String token = req.getHeader("Authorization");
        if (token != null) {
            // Retrieve session from database
            Session s = Session.findByToken(token, em);
            if (s != null) {
                // Set user in the request
                req.setAttribute("user", s.getOwner());
                // Return user information
                response.add("user", s.getOwner().toJson());
                return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
            }
        }
        response.add("error", "Not logged in");
        return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest req, String body) {
        JsonObjectBuilder response = Json.createObjectBuilder();
        JsonObject data = Json.createReader(new StringReader(body)).readObject();
        String displayName = data.getString("displayName");
        // Get the authentication bearer token from the header
        String token = req.getHeader("Authorization");
        if (token != null) {
            // Retrieve session from database
            Session s = Session.findByToken(token, em);
            if (s != null) {
                /// Get the user from the session
                User user = s.getOwner();
                // Update the user's display name
                user.setDisplay_name(displayName);
                em.persist(user);
                // Return user information
                response.add("user", user.toJson());
                return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
            }
        }
        response.add("error", "Not logged in");
        return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
    }

    @GET
    @Path("/login")
    public Response login(@QueryParam("ticket") String ticket, @Context HttpServletRequest req) {
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Get the cookie from the header
        String token = req.getHeader("Authorization");
        if (token != null) {
            Session s = Session.findByToken(token, em);
            if (s != null) {
                // Set user in the request
                req.setAttribute("user", s.getOwner());
                // Return user information
                response.add("user", s.getOwner().toJson());
                return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
            }
        }
        // Retrieve user information from CAS
        String casUrl = CAS_URL + "/serviceValidate?service=" + CAS_SERVICE + "&ticket=" + ticket;
        try {
            // Send request to CAS
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(casUrl);
            // Check if CAS response is valid
            if (document.getElementsByTagName("cas:authenticationFailure").getLength() > 0) {
                response.add("error", "Invalid ticket");
                return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
            }
            // Parse XML response
            String uid = document.getElementsByTagName("cas:user").item(0).getTextContent();
            String FirstName = document.getElementsByTagName("cas:first_name").item(0).getTextContent();
            String LastName = document.getElementsByTagName("cas:last_name").item(0).getTextContent();
            // Check if user exists in database (based on username)
            User user = em.find(User.class, uid);
            if (user == null) {
                // If not, create a new user
                user = new User(uid, FirstName + " " + LastName);
                em.persist(user);
            }
            // Create a new session
            Session s = new Session(user);
            em.persist(s);
            // Set cookie in the header
            response.add("user", user.toJson());
            return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).cookie(new NewCookie("session", s.getToken(), "/", null, null, 3600, false)).build();
        } catch (ParserConfigurationException | org.xml.sax.SAXException | IOException e) {
            // Handle URL and IO exceptions
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/logout")
    public Response logout(@Context HttpServletRequest req) {
        // Get the cookie from the header
        String token = req.getHeader("Authorization");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Check if session is valid
        if (token != null) {
            Session s = Session.findByToken(token, em);
            if (s != null) {
                em.remove(s);
                return Response.ok().build();
            }
        }
        response.add("error", "Invalid session");
        return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
    }

    // For testing purposes
    @GET
    @Path("/loginCAS")
    public Response loginCAS() {
        return Response.seeOther(URI.create(CAS_URL + "/login?service=" + CAS_SERVICE)).build();
    }
}