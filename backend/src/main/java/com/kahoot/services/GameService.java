package com.kahoot.services;

import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.json.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.net.URI;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.StringReader;

import java.util.List;

import static java.lang.Math.min;
import static java.lang.Math.floor;
// Import ZoneOffset and OffsetDateTime
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;


import javax.servlet.http.HttpServletRequest;

// import User Bean and Session Bean
import com.kahoot.beans.User;
import com.kahoot.beans.Session;
import com.kahoot.beans.Game;
import com.kahoot.beans.Question;
import com.kahoot.beans.Answer;
import com.kahoot.beans.UserAnswer;

@Singleton
@Path("/game")
public class GameService {

    @PersistenceContext
    private EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve games from database
        List<Game> games = em.createQuery("SELECT g FROM Game g WHERE g.owner = :user", Game.class).setParameter("user", user).getResultList();
        JsonArrayBuilder gamesJson = Json.createArrayBuilder();
        for (Game game : games) {
            gamesJson.add(game.toJson());
        }
        response.add("games", gamesJson);
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response get(@PathParam("id") String id, @Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        response.add("game", game.toJson());
        response.add("user", user.toJson());
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/join")
    public Response join(@PathParam("id") String id, @Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        // Check is game is STARTED
        if (game.getStatus() == Game.Status.STARTED) {
            // Check if user is already in game
            if (!game.getPlayers().contains(user) && !game.getOwner().getUid().equals(user.getUid())) {
                // Add current user to played_game
                if (game.getPlayers().add(user)) {
                    em.persist(game);
                }
            }
        }
        response.add("game", game.toJson());
        response.add("user", user.toJson());
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(String body, @Context HttpServletRequest req) {
        System.out.println("Create game");
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        JsonObject data = Json.createReader(new StringReader(body)).readObject();
        String name = data.getString("name");
        JsonArray questions = data.getJsonArray("questions");
        // Check if name is valid
        if (name == null || name.length() == 0) {
            response.add("error", "Invalid name");
            return Response.status(Response.Status.BAD_REQUEST).entity(response.build().toString()).build();
        }
        // Create a new game
        Game game = new Game(name, user);
        em.persist(game);
        // Add questions to database
        if (questions != null) {
            for (int i = 0; i < questions.size(); i++) {
                Question question = new Question(questions.getJsonObject(i).getString("question"), i, game);
                em.persist(question);
                JsonArray answers = questions.getJsonObject(i).getJsonArray("answers");
                if (answers != null) {
                    for (int j = 0; j < min(answers.size(), 4); j++) {
                        System.out.println(answers.getJsonObject(j).getString("answer"));
                        System.out.println(answers.getJsonObject(j).getBoolean("is_correct"));
                        Answer answer = new Answer(answers.getJsonObject(j).getString("name"), answers.getJsonObject(j).getBoolean("is_correct"), question);
                        em.persist(answer);
                    }
                }
            }
        }
        // Return the game
        response.add("game", game.getId());
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") String id, String body, @Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        JsonObject data = Json.createReader(new StringReader(body)).readObject();
        String name = data.getString("name");
        JsonArray questions = data.getJsonArray("questions");
        // Check if name is valid
        if (name == null || name.length() == 0) {
            response.add("error", "Invalid name");
            return Response.status(Response.Status.BAD_REQUEST).entity(response.build().toString()).build();
        }
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to update game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        // Update game
        game.setName(name);
        em.persist(game);
        
        for (Question question : game.getQuestions()) {
            // Remove questions
            System.out.println("Removing question " + question.getId());
            em.remove(question);
            em.flush();
        }
        // Add questions to database
        if (questions != null) {
            // Add new questions
            for (int i = 0; i < questions.size(); i++) {
                Question question = new Question(questions.getJsonObject(i).getString("question"), i, game);
                Integer timeLimit = questions.getJsonObject(i).getInt("time_limit");
                if (timeLimit != null && timeLimit > 0) {
                    question.setTime_limit(timeLimit);
                }
                em.persist(question);
                
                JsonArray answers = questions.getJsonObject(i).getJsonArray("answers");
                if (answers != null) {
                    for (int j = 0; j < min(answers.size(), 4); j++) {
                        Answer answer = new Answer(answers.getJsonObject(j).getString("answer"), answers.getJsonObject(j).getBoolean("is_correct"), question);
                        em.persist(answer);
                    }
                }
            }
        }
        response.add("success", true);
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id, @Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to delete game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        // Delete game
        em.remove(game);
        response.add("success", "Game deleted");
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("{id}/answer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(@PathParam("id") String id, String body, @Context HttpServletRequest req) {
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        JsonArray answers = Json.createReader(new StringReader(body)).readArray();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (game.getStatus() != Game.Status.PLAYING) {
            response.add("error", "Game not started");
            return Response.status(Response.Status.BAD_REQUEST).entity(response.build().toString()).build();
        }
        if (answers == null) {
            response.add("error", "Answers not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(response.build().toString()).build();
        }
        double score = 0.0;
        for (int i = 0; i < answers.size(); i++) {
            Answer answer = em.find(Answer.class, answers.getInt(i));
            if (answer == null) {
                response.add("error", "Answer not found");
                return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
            }
            UserAnswer userAnswer = new UserAnswer(user, answer);
            em.persist(userAnswer);
            if (answer.is_correct()) {
                // Get the time difference between the question and the answer in seconds
                OffsetDateTime questionTime = answer.getQuestion().getDate();
                OffsetDateTime answerTime = userAnswer.getDate();
                double timeDiff = (double) (answerTime.toEpochSecond() - questionTime.toEpochSecond());
                Integer timeLimit = answer.getQuestion().getTime_limit();
                score += (1 - ((timeDiff / timeLimit) / 2)) * 1000;
            } else {
                response.add("score", 0);
                return Response.status(Response.Status.CREATED).entity(response.build().toString()).build();
            }
        }
        response.add("score", floor(score));
        return Response.status(Response.Status.CREATED).entity(response.build().toString()).build();
    }

    @POST
    @Path("{id}/start")
    @Produces(MediaType.APPLICATION_JSON)
    public Response start(@PathParam("id") String id, @Context HttpServletRequest req) {
        System.out.println("Start game " + id);
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to start the game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        // Start game
        game.setDate(OffsetDateTime.ofInstant(new Date().toInstant(), ZoneOffset.UTC));
        game.setStatus(Game.Status.STARTED);
        em.persist(game);
        response.add("success", "Game started");
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("{id}/play")
    @Produces(MediaType.APPLICATION_JSON)
    public Response play(@PathParam("id") String id, @Context HttpServletRequest req) {
        System.out.println("Play game " + id);
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to start the game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        // Start game
        game.setStatus(Game.Status.PLAYING);
        em.persist(game);
        Question question = game.getQuestions().get(0);
        question.setDate(OffsetDateTime.now());
        em.persist(question);
        response.add("success", "Game playing");
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("{id}/next")
    @Produces(MediaType.APPLICATION_JSON)
    public Response next(@PathParam("id") String id, @Context HttpServletRequest req) {
        System.out.println("Next question " + id);
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to start the game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        // Next question
        game.setCurrent_question(game.getCurrent_question() + 1);
        em.persist(game);
        Question question = game.getQuestions().get(game.getCurrent_question());
        if (question != null) {
            question.setDate(OffsetDateTime.now());
            em.persist(question);
        }
        response.add("score", "Game playing");
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("{id}/end")
    @Produces(MediaType.APPLICATION_JSON)
    public Response end(@PathParam("id") String id, @Context HttpServletRequest req) {
        System.out.println("End game " + id);
        User user = (User) req.getAttribute("user");
        JsonObjectBuilder response = Json.createObjectBuilder();
        // Retrieve game from database
        Game game = em.find(Game.class, id);
        if (game == null) {
            response.add("error", "Game not found");
            return Response.status(Response.Status.NOT_FOUND).entity(response.build().toString()).build();
        }
        if (!user.is_admin() && !game.getOwner().getUid().equals(user.getUid())) {
            response.add("error", "User not authorized to end the game");
            return Response.status(Response.Status.UNAUTHORIZED).entity(response.build().toString()).build();
        }
        game.setStatus(Game.Status.FINISHED);
        em.persist(game);
        response.add("success", "Game stopped");
        return Response.ok(response.build().toString(), MediaType.APPLICATION_JSON).build();
    }
}