# Projet Web

Application de quizz style kahoot

# Endpoint

/rest/ -> page d'accueil

/rest/login -> se connecter

/rest/logout -> se déconnecter

/rest/profile -> éditer ses infos (display Name)

/rest/game/create -> créer une partie

/rest/game/\[uid\]

- si Admin ou créateur -> gérer la partie
- sinon interface joueur avec socket

/rest/game/\[uid\]/live -> Démarre la partie et ouvre un socket

/rest/game/\[uid\]/question -> Ajouter/Modifier/Supprimer question

/rest/game/\[uid\]/answer -> Ajouter/Modifier/Supprimer réponse
