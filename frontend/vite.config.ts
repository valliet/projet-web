import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import { webSocketServer } from './svelte.config';

export default defineConfig({
	plugins: [sveltekit(), webSocketServer],
});
