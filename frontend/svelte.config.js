import adapter from '@sveltejs/adapter-node';
import { vitePreprocess } from '@sveltejs/kit/vite';

import { Server } from 'socket.io';
// Get from env
const API_URL = process.env.PRIVATE_API_URL || 'http://localhost:8080';

export const webSocketServer = {
	// This is the socket.io server instance.
	name: 'webSocketServer',
	configureServer(server) {
		const io = new Server(server.httpServer);
		io.on('connect', (socket) => {
			console.log('Client connected!', socket.id);

			socket.on('game', async (data) => {
				const game_id = data.game_id;
				fetch(`${API_URL}/rest/game/${game_id}/join`, {
					method: 'GET',
					headers: {
						Authorization: `${data.auth}`
					}
				}).then((response) => {
					response.json().then((data) => {
						if (data.game.status === 'STARTED') {
							socket.join(game_id);
							console.log('Game join!', game_id);
							io.to(game_id).emit('join', data.user.display_name);
						}
					});
				});
			});

			socket.on('play', async (data) => {
				console.log('Start the game', data);
				const game_id = data.game_id;
				const auth = data.auth;
				// Fetch game from API
				const response = await fetch(`${API_URL}/rest/game/${game_id}/play`, {
					method: 'POST',
					headers: {
						Authorization: `${auth}`
					}
				});
				if (response.ok) {
					io.to(game_id).emit('play', data);
				}
			});

			socket.on('next', async (data) => {
				console.log('Next', data);
				const game_id = data.game_id;
				const auth = data.auth;
				// Fetch game from API
				const response = await fetch(`${API_URL}/rest/game/${game_id}/next`, {
					method: 'POST',
					headers: {
						Authorization: `${auth}`
					}
				});
				if (response.ok) {
					io.to(game_id).emit('next', data);
				}
			});

			socket.on('answer', async (data) => {
				console.log('Answer', data);
				const game_id = data.game_id;
				const auth = data.auth;
				const answers = data.answers;
				// Fetch game from API
				const response = await fetch(`${API_URL}/rest/game/${game_id}/answer`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `${auth}`
					},
					body: JSON.stringify(answers)
				});
				if (response.ok) {
					const score = await response.json();
					console.log('Score', score);
					socket.emit('score', score);
				}
			});

			socket.on('finish', async (data) => {
				console.log('finish', data);
				const game_id = data.game_id;
				const auth = data.auth;
				// Fetch game from API
				const response = await fetch(`${API_URL}/rest/game/${game_id}/end`, {
					method: 'POST',
					headers: {
						Authorization: `${auth}`
					}
				});
			});
		});
	}
};

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: vitePreprocess(),

	kit: {
		// adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
		// If your environment is not supported or you settled on a specific environment, switch out the adapter.
		// See https://kit.svelte.dev/docs/adapters for more information about adapters.
		adapter: adapter()
	}
};

export default config;
