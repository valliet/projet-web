import { PRIVATE_API_URL } from '$env/static/private';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ cookies }) => {
	const response = await fetch(`${PRIVATE_API_URL}/rest/user/logout`, {
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});

	// Remove the session cookie
	await cookies.delete('session');
	// Redirect to the home page
	return new Response(null, {
		status: 302,
		headers: {
			Location: 'http://localhost:5173/'
		}
	});
};
