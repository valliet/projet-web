import { PRIVATE_API_URL } from '$env/static/private';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ url, cookies }) => {
	const ticket = url.searchParams.get('ticket');

	const response = await fetch(`${PRIVATE_API_URL}/rest/user/login?ticket=${ticket}`, {
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});

	const cookieValue = await response.headers.get('set-cookie');

	if (cookieValue) {
		const responseHeaders = {
			'Set-Cookie': cookieValue,
			Location: 'http://localhost:5173/'
		};

		return new Response(null, {
			status: 302,
			headers: responseHeaders
		});
	}

	return new Response(null, {
		status: 302,
		headers: {
			Location: 'http://localhost:5173/'
		}
	});
};
