import type { Actions } from './$types';
import { PRIVATE_API_URL } from '$env/static/private';
import { redirect } from '@sveltejs/kit';

export const actions: Actions = {
	default: async ({ request, cookies }) => {
		const data = await request.formData();
		const displayName = data.get('displayName');
		const response = await fetch(`${PRIVATE_API_URL}/rest/user`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${cookies.get('session')}`
			},
			body: JSON.stringify({
				displayName: displayName
			})
		});
		const { user } = await response.json();
		throw redirect(302, `/profile`);
	}
};
