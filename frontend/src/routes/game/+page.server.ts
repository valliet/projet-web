import type { PageServerLoad } from '../$types';
import { PRIVATE_API_URL } from '$env/static/private';

export const load: PageServerLoad = async ({ cookies }) => {
	// Get the current user from the private API
	// By passing the cookie in the "Authorization: Bearer" header
	const response = await fetch(`${PRIVATE_API_URL}/rest/game`, {
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});
	// Parse the json response
	let { games } = await response.json();
	// Return the user
	return { games };
};
