import type { Actions } from './$types';
import { PRIVATE_API_URL } from '$env/static/private';
import { redirect } from '@sveltejs/kit';

export const actions: Actions = {
    async startGame({ params, cookies }) {
        const { id } = params;
        const response = await fetch(`${PRIVATE_API_URL}/rest/game/${id}/start`, {
            method: 'POST',
            headers: {
                Authorization: `${cookies.get('session')}`
            }
        });
        let { success } = await response.json();
        if (success) {
            throw  redirect(302, `/game/${id}`);
        }
    },
};