import { PRIVATE_API_URL } from '$env/static/private';
import type { RequestHandler } from './$types';

export const PUT: RequestHandler = async ({ params, cookies, request }) => {
	const { id } = params;
	const { game } = await request.json();
	const response = await fetch(`${PRIVATE_API_URL}/rest/game/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${cookies.get('session')}`
		},
		body: JSON.stringify({
			name: game.name,
			questions: game.questions
		})
	});
	return response;
};
