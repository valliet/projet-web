import { PRIVATE_API_URL } from '$env/static/private';
import type { RequestHandler } from './$types';

export const DELETE: RequestHandler = async ({ params, cookies }) => {
	const { id } = params;
	const response = await fetch(`${PRIVATE_API_URL}/rest/game/${id}`, {
		method: 'DELETE',
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});
	return response;
};