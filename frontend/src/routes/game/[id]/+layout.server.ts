import { PRIVATE_API_URL } from '$env/static/private';
import type { PageLayoutLoad } from './$types';

export const load: PageLayoutLoad = async ({ cookies, params }) => {
	// Get the current user from the private API
	// By passing the cookie in the "Authorization: Bearer" header
	const response = await fetch(`${PRIVATE_API_URL}/rest/game/${params.id}`, {
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});
	// Parse the json response
	let { game } = await response.json();
	// Return the user
	return { game };
};
