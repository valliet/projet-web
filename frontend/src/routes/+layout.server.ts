import { PRIVATE_API_URL } from '$env/static/private';
import type { User } from '$lib/user';
import type { PageLayoutLoad } from './$types';

export const load: PageLayoutLoad = async ({ cookies }) => {
	// Get the current user from the private API
	// By passing the cookie in the "Authorization: Bearer" header
	const response = await fetch(`${PRIVATE_API_URL}/rest/user`, {
		headers: {
			Authorization: `${cookies.get('session')}`
		}
	});
	// Parse the json response
	const { user } = (await response.json()) as { user: User };
	// Return the user
	return { user };
};
