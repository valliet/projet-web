import type { Actions } from './$types';
import { PRIVATE_API_URL } from '$env/static/private';
import { redirect } from '@sveltejs/kit';

export const actions: Actions = {
	default: async ({ request, cookies }) => {
		const data = await request.formData();
		const title = data.get('name');
		const response = await fetch(`${PRIVATE_API_URL}/rest/game/create`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${cookies.get('session')}`
			},
			body: JSON.stringify({
				name: title
			})
		});
		const { game } = await response.json();
		// Redirect to the new game
		throw redirect(302, `/game/${game}/edit`);
	}
};
