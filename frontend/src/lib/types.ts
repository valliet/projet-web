import type { User } from './user';

export type Answer = { id?: number; answer: string; is_correct: boolean };
export type Question = {
	id?: number;
	question: string;
	answers: Answer[];
	date?: string;
	time_limit?: number;
};
export type Game = { uid: string; name: string; owner: User; questions: Question[] };
